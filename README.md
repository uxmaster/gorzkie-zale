# Gorzkie Żale

Aby przeglądać, kliknij 👉[Gorzkie Żale](https://uxmaster.gitlab.io/gorzkie-zale/).


## Wyrazy uznania dla

* [Jesus icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/jesus), za ikonkę projektu;
* Mateo Cerezo (1637 - 1666), który namalował obraz w tle strony, pobrany z [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Cristo_de_la_Sangre._Mateo_Cerezo_el_Joven,_Museo_de_Burgos.jpg).


## Kontakt

Ewentualne uwagi i błędy proszę zgłaszać [tutaj](https://gitlab.com/uxmaster/gorzkie-zale/-/issues).