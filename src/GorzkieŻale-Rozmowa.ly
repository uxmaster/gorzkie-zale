% This LilyPond file was downladed from https://drStypula.pl/gz
% generated in Rosegarden and corrected in Frescobaldi
\version "2.12.2"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)
\header {
  %composer = "melodia tradycyjna"
    subtitle = \markup{ \medium \fontsize #1.5 "Rozmowa duszy z Matką bolesną" }
    tagline = "https://drStypula.pl/gz"
    %title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"
global = {
    \time 4/4
    \skip \breve.*3  %% 1-1
}
globalTempo = {
    \tempo 4 = 70  \skip \breve.*2 \skip 1*2 \skip 2
}
\score {
    \new StaffGroup <<
        % force offset of colliding notes in chords:
        \override Score.NoteColumn #'force-hshift = #1.0

        \context Staff = "track 1" <<
          % \set Staff.instrumentName = \markup { \column { "Głos " } }
          % \set Score.skipBars = ##t
          % \set Staff.printKeyCancellation = ##f
            \new Voice \global
            \new Voice \globalTempo

            \context Voice = "voice 1" {
              %   \override Voice.TextScript #'padding = #2.0
              %\override MultiMeasureRest #'expand-limit = 1

%\time 4/4
                \clef "treble"
                \key e \major
                gis'8 _(fis') a'4 gis' gis' 
                a'8 _(gis') b'4 fis'8 _(a') gis'4 
                b'4 cis''8 _(b') a'4 a' b' b' fis' fis' 
                gis' fis' b' b'8_(a') gis'_(fis') b'_(a') gis'4 r 
                fis' fis' b' b'8_(a') gis'_(fis') b'_(a') gis'4 r
                \bar"|."
              } % Voice
            \addlyrics {
              \set stanza = #"1." Ach! Ja Ma -- tka tak ża -- ło -- sna! Bo -- leść Mnie ści -- ska nie -- znoś -- na, Miecz Me ser -- ce prze -- ni -- ka, Miecz Me ser -- ce prze -- ni -- ka.
            }
%             \addlyrics {
%                 Świę -- ta Pan -- no, u -- proś dla mnie, Bym ran Sy -- na Twe -- go zna -- mię, Miał na ser -- cu wy -- ry -- te!, Miał na ser -- cu wy -- ry -- te!
%             }
        >> % Staff (final)
    >> % notes
  \layout {indent = 0\cm}
  \midi { }

} % score
