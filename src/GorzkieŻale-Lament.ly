% This LilyPond file was downladed from https://drStypula.pl/gz
% generated in Rosegarden and corrected in Frescobaldi
\version "2.12.2"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)
\header {
  %composer = "melodia tradycyjna"
    subtitle = \markup{ \medium \fontsize #1.5 "Lament duszy nad cierpiącym Jezusem" }
    %tagline = "https://drStypula.pl/gz"
    %title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"
global = {
    %\time 2/4
    %\skip \breve.*3  %% 1-1
}
globalTempo = {
    \tempo 4 = 60  \skip \breve.*2 \skip 1*2 \skip 2
}
\score {
    \new StaffGroup <<
        % force offset of colliding notes in chords:
        \override Score.NoteColumn #'force-hshift = #1.0

        \context Staff = "track 1" <<
          % \set Staff.instrumentName = \markup { \column { "Głos " } }
            \set Score.skipBars = ##t
            \set Staff.printKeyCancellation = ##f
            \new Voice \global
            \new Voice \globalTempo

            \context Voice = "voice 1" {
                \override Voice.TextScript #'padding = #2.0
                \override MultiMeasureRest #'expand-limit = 1

                \time 1/8
                \clef "treble"
                \key e \major
                \cadenzaOn
                e' 8 _(fis') gis' 4 \bar "|"
                gis' 8 gis' a' 4 gis' 8 gis' fis' gis' 4 \bar "|"
                fis' fis' 8 fis' gis' gis' gis' gis'4 fis'8 e' fis' _(a') gis' 4 \bar "|"
                b' 4 b' a' gis' 8 _(fis') b' _(a') gis' 4
                %\cadenzaOff
            } % Voice
            \addlyrics {
                \set stanza = #"1." Je -- zu, na za -- bi -- cie o -- krut -- ne, Ci -- chy Ba -- ran -- ku od wro -- gów szu -- ka -- ny, Je -- zu mój ko -- cha -- ny!
            }
%             \addlyrics {
%                 Je -- zu, od zło -- śli -- wych mor -- der -- ców, Po śli -- cznej twa -- rzy tak spro -- śnie ze -- plwa -- ny, Je -- zu mój ko -- cha -- ny!
%             }
        >> % Staff (final)
    >> % notes

\layout {indent = 0\cm
\context {
          \Staff
          \remove "Time_signature_engraver"
%           \remove "Bar_engraver"
%           \override Stem #'transparent = ##t
        }
}
\midi { }

} % score
