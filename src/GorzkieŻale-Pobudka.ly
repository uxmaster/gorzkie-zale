% This LilyPond file was downladed from https://drStypula.pl/gz
% generated in Rosegarden and corrected in Frescobaldi
\version "2.12.2"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)
\header {
    composer = "melodia tradycyjna"
    subtitle = \markup{ \medium \fontsize #1.5 "Pobudka" }
    tagline = "https://drStypula.pl/gz"
    title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"

global = { 
    \time 4/4
    \skip 1*6  %% 1-6
}
globalTempo = {
    \tempo 4 = 60  \skip 1*6 
}

\score {
    \new StaffGroup <<
        % force offset of colliding notes in chords:
        \override Score.NoteColumn #'force-hshift = #1.0

        \context Staff = "track 1" << 
            %\set Staff.instrumentName = \markup { \column { "Głos " } }
            %\set Score.skipBars = ##t
            %\set Staff.printKeyCancellation = ##f
            \new Voice \global
            \new Voice \globalTempo

            \context Voice = "voice 2" {
              %\override Voice.TextScript #'padding = #2.0
              %\override MultiMeasureRest #'expand-limit = 1
                \time 4/4
                \clef "treble"
                \key cis \minor
                e' 8 dis' fis' 4 e' 8 dis' cis' 4  |
                gis' 8 fis' e' 4 dis' cis'  |
                gis' 4 gis' fis' fis'  |
                gis' 4 fis' e' dis'  |
%% 5
                e' 8 dis' fis' 4 e' 8 dis' cis' 4  |
                gis' 8 fis' e' 4 dis' cis'  |
                \bar "|."
         } % Voice
         \addlyrics {
                  \set stanza = #"1." Gorz -- _ kie ża - le, przy - by -- waj -- cie, Ser -- ca na -- sze prze -- ni -- kaj -- cie, Ser -- _ ca na -- _ sze prze -- _ ni -- kaj -- cie.
                }
                %   \addlyrics {
                %  \set stanza = #"2." Roz -- _ płyń -- cie _ się, me _ źre -- ni -- ce, Tocz -- cie smu -- tnych łez kry -- ni -- ce, Tocz -- _ cie smu -- _ tnych łez _ kry -- ni -- ce.
              %}
        >> % Staff (final)
    >> % notes
    \layout {indent = 0.0\cm} 
    \midi { }
} % score
