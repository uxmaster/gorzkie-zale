\version "2.12.3"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)

\header {
  %composer = "melodia tradycyjna"
    subtitle = \markup{ \medium \fontsize #1.5 "Rozmowa duszy z Matką bolesną" }
    tagline = "https://drStypula.pl/gz"
    %title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"
global = {
  \key e \major \autoBeamOff
  \time 4/4
}
globalTempo = {
    \tempo 4 = 80  \skip \breve.*2 \skip 1*2 \skip 2 
}
verse =  \lyricmode {
              \set stanza = #"1." Ach! Ja Ma -- tka tak ża -- ło -- sna! Bo -- leść Mnie ści -- ska nie -- znoś -- na, Miecz Me ser -- ce prze -- ni -- ka, Miecz Me ser -- ce prze -- ni -- ka.
                   }
                   
       
              
rightOne =  {
                gis'8 [fis'] a'4 gis' gis' 
                a'8 [gis'] b'4 fis'8 [a'] gis'4 
                b'4 cis''8 [b'] a'4 a' b' b' fis' fis' 
                gis'4 fis' b' b'8 [a'] gis' [fis'] b' [a'] gis'2 
                fis'4 fis' b' b'8 [a'] gis' [fis'] b' [a'] gis'2
                \bar"|."
  
}

rightTwo =  {
                e'1
                fis'2 dis'4 e'
                gis' a'8 [gis'] fis' [e'] dis'4
                gis' (gis') e'8[dis'16\trill cis'] dis'4
                e' dis'2 (dis'4)
                e' dis' e'2
                e'4 dis'2 (dis'4)
                e' <<fis' dis'>> <<e'2 b2>> s1

}

leftOne =  {
                b1
                fis4 gis4 b2
                (b4) fis (fis) b
                b2 (b4) (b8) [a]
                b1
                (b2) (b)
                b1
                (b4)
}

leftTwo =  {
                e4 fis e8 [dis] cis4
                (cis) dis fis e8 [dis]
                (dis4) cis (cis) fis
                e8 [dis] fis4 b, (b,)
                e4 fis gis fis
                e fis e2
                fis gis4 fis
                e b, e,2 s1

}

\score {
  \new PianoStaff <<
    

    
    \new Staff = "right" \with {
      midiInstrument = "piccolo"
    } << 
      \new Voice = "One" { \voiceOne <<     \globalTempo \global \rightOne >> }
      \new Voice = "Two" { \voiceTwo << \global \rightTwo >> }
    >>
    \new Lyrics \with { alignAboveContext = right } \lyricsto One \verse
    %\new Lyrics \with { alignAboveContext = right } \lyricsto One \verserep
    \new Staff = "left" \with {
      midiInstrument = "piccolo"
    } { \clef bass << \global \leftOne \\ \global \leftTwo >> }
  >>
  \layout {indent = 0\cm
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
    %\context {
    %                    \Staff
    %                    \remove "Time_signature_engraver"
                        %\remove "Bar_engraver"
                        %\override Stem #'transparent = ##t
                        %            }
  }
  \midi {
    %\context {
    %  \Score
    %  tempoWholesPerMinute = #(ly:make-moment 60 4)
  %}
  }
}
