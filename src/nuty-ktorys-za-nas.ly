% This LilyPond file was downladed from https://uxmaster.gitlab.io/gorzkie-zale/
% generated in Rosegarden and corrected in Frescobaldi
\version "2.12.2"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)

\include "cantate_style.ly"
global = {
    %\time 2/4
    %\skip \breve.*3  %% 1-1
}
globalTempo = {
    \tempo 4 = 45  \skip \breve.*2 \skip 1*2 \skip 2
}
\score {
    \new StaffGroup <<
        % force offset of colliding notes in chords:
        \override Score.NoteColumn #'force-hshift = #1.0

        \context Staff = "track 1" <<
          % \set Staff.instrumentName = \markup { \column { "Głos " } }
            \set Score.skipBars = ##t
            \set Staff.printKeyCancellation = ##f
            \new Voice \global
            %\new Voice \globalTempo

            \context Voice = "voice 1" {
                \override Voice.TextScript #'padding = #2.0
                \override MultiMeasureRest #'expand-limit = 1

                \time 1/8
                \clef "treble"
                \key e \minor
                \cadenzaOn
                e'8 fis' e' dis' e' fis' g'4 fis'4 \breathe
                b'4 a' g'8_(fis') e'4 \breathe
                b'8 b' a' g' fis'4^\markup{ \italic rit. } e' \bar "|."
                %\cadenzaOff
            } % Voice
            \addlyrics {
                Któ -- ryś za nas cier -- piał ra -- ny, Je -- zu Chry -- ste zmi -- łuj się nad na -- mi.
            }
        >> % Staff (final)
    >> % notes

\layout {indent = 0\cm
\context {
          \Staff
          \remove "Time_signature_engraver"
%           \remove "Bar_engraver"
%           \override Stem #'transparent = ##t
        }
}
\midi { }

} % score
