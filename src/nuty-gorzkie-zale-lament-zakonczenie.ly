% This LilyPond file was downladed from https://uxmaster.gitlab.io/gorzkie-zale/
% generated in Rosegarden and corrected in Frescobaldi
\version "2.12.2"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)

\include "cantate_style.ly"
global = {
    %\time 2/4
    %\skip \breve.*3  %% 1-1
}
globalTempo = {
  %\tempo 4 = 60  \skip \breve.*2 \skip 1*2 \skip 2
}
\score {
    \new StaffGroup <<
        % force offset of colliding notes in chords:
        \override Score.NoteColumn #'force-hshift = #1.0

        \context Staff = "track 1" <<
          % \set Staff.instrumentName = \markup { \column { "Głos " } }
            \set Score.skipBars = ##t
            \set Staff.printKeyCancellation = ##f
            \new Voice \global
            %\new Voice \globalTempo

            \context Voice = "voice 1" {
                \override Voice.TextScript #'padding = #2.0
                \override MultiMeasureRest #'expand-limit = 1

                \time 1/8
                \clef "treble"
                \key e \major
                \cadenzaOn
                b' 4 a' gis' 8 _(fis') b' _(a') gis' 4 \bar "|"
                b' 4 a' gis' 8 _(fis') b' _(a') gis' 4 \bar "|"
                e' 4 e' fis' fis' 8 _(a') gis' 4 \bar "|"
                e' 4 e' fis' fis' 8 _(a') gis' 4 \bar "|"
                b' 4 a' gis' 8 _(fis') b' _(a') gis' 4 \bar "|"
                b' 4 a' gis' 8 _(fis') b' _(a') gis' 4 \bar "|"
                gis'8^\markup{ \italic rall. } gis' fis' e' fis'_(a') gis' 4 \bar "|."
                \cadenzaOff
            } % Voice
            \addlyrics {
               Bądź po -- zdro -- wio -- ny, bądź po -- chwa -- lo -- ny, dla nas zel -- żo -- ny i po -- hań -- bio -- ny!
Bądź u -- wiel -- bio -- ny, bądź wy -- sła -- wio -- ny, Bo -- że nie -- skoń -- czo -- ny!

            }
        >> % Staff (final)
    >> % notes

\layout {indent = 0\cm
\context {
          \Staff
          \remove "Time_signature_engraver"
%           \remove "Bar_engraver"
%           \override Stem #'transparent = ##t
        }
}
\midi { }

} % score
