\version "2.12.3"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)

\header {
  %composer = "melodia tradycyjna"
    subtitle = \markup{ \medium \fontsize #1.5 "Hymn" }
    tagline = "https://drStypula.pl/gz"
    %title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"
global = {
  \key cis \minor \autoBeamOff
  %\time 4/4
}
globalTempo = {
    \tempo 4 = 80  \skip \breve.*2 \skip 1*2 \skip 2 
}
verse =  \lyricmode {
                     \set stanza = #"1." Żal du -- szę ści -- ska, ser -- ce bo -- leść -- czu -- je, Klę -- czy "w O" -- grój -- cu, gdy krwa -- wy pot le -- je, me ser -- ce mdle -- je.
                   }
                   
verserep =   \lyricmode{ 
                 Gdy sło -- dki Je -- zus na śmierć się go -- tu -- je;
                  }
           
              
rightOne =  {
              \cadenzaOn
  gis'4 gis' g' gis'8 [dis'] fis'2 \breathe e'4 dis' cis' fis'8 [e'] dis' [cis'] b2 \bar ":|" gis'8 [b'] b'4 a' fis'8 [a'] gis'2 \breathe gis'4 fis' e'4 a' fis'8 [a'] gis'2 \breathe gis'4 fis' fis'8 [e'] dis' [cis'] b2 
  s2 \bar "|."
                \cadenzaOff
  
}

rightTwo =  {
        s1. s1. s4
        e'8 gis' fis'2 dis'4 b2
        e'4 cis'2. dis'4 b2
        e'4 cis'2
}

leftOne =  {
        \parenthesize gis4 ais1(ais1) fis4 gis(gis2) \bar ":|"
        s1. s1. s1
        cis4(dis2)
  
}

leftTwo =  {
        \parenthesize dis4(dis2.)(dis4) cis1. dis2 \bar ":|"
        \ottava #-1
        \set Staff.ottavation = #"8"
        b,8 e, dis,2 b,8 fis, e,4 dis,
        cis, a,2 fis,4 b,8 fis, e,4 dis,
        cis,
        \ottava #0
        a,2 gis,4(gis,2)
        s2
}

\score {
  \new PianoStaff <<
    
    
    
    \new Staff = "right" \with {
      midiInstrument = "piccolo"
    } << 
      \new Voice = "One" { \voiceOne << \globalTempo \global \rightOne >> }
      \new Voice = "Two" { \voiceTwo << \global \rightTwo >> }
    >>
    \new Lyrics \with { alignAboveContext = right } \lyricsto One \verse
    \new Lyrics \with { alignAboveContext = right } \lyricsto One \verserep
    \new Staff = "left" \with {
      midiInstrument = "piccolo"
    } { \clef bass << \global \leftOne \\ \global \leftTwo >> }
  >>
  \layout {indent = 0\cm
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
              \context {
                        \Staff
                        \remove "Time_signature_engraver"
                        %\remove "Bar_engraver"
                        %\override Stem #'transparent = ##t
                }
  }
  \midi {
    %\context {
    %  \Score
    %  tempoWholesPerMinute = #(ly:make-moment 60 4)
  %}
  }
}
