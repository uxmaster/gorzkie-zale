%% cantate_style.ly -- style from https://uxmaster.gitlab.io/kawomu.pl/

%--- size
%#(set-global-staff-size 23)
%#(set-global-staff-size 22) % dopasowane do LaTeXa
#(set-global-staff-size 19)
%#(set-default-paper-size "a4")

\layout {
  %--- nice beam slope
%   \context { \Voice
%     \override Beam  #'damping = #0
%     \override Stem #'details #'beamed-lengths =  #'(3.5 3.5 3.5)
%     \override Beam  #'positions =
%     #(ly:make-simple-closure
%       (ly:make-simple-closure
%       (list chain-grob-member-functions
%         `(,cons 0 0)
%         ly:beam::calc-least-squares-positions
%         ly:beam::slope-damping
%         ly:beam::shift-region-to-valid
%       )))
%   }
  %--- To match LaTeX font size
   \context {
     \Lyrics
     \override LyricText #'font-size = #-0.1
   }
  %--- BarLine (evince & okular patch)
      \context { \Staff
          \override BarLine #'hair-thickness = #1.0
          \override BarLine #'thick-thickness = #5.0
          \override BarLine #'kern = #4.0
   }
  \context {
    \Score
    \remove "Bar_number_engraver"
  }
}

       