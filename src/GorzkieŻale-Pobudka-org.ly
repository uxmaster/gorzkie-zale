\version "2.12.3"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)

\header {
    composer = "melodia tradycyjna"
    subtitle = \markup{ \medium \fontsize #1.5 "Pobudka" }
    tagline = "https://drStypula.pl/gz"
    title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"
global = {
  \key cis \minor
  \time 4/4
}
globalTempo = {
    \tempo 4 = 60  \skip 1*6 
}
verse =  \lyricmode {
                  \set stanza = #"1." Gorz -- _ kie ża - le, przy - by -- waj -- cie, Ser -- ca na -- sze prze -- ni -- kaj -- cie, Ser -- _ ca na -- _ sze prze -- _ ni -- kaj -- cie.
                }
                %   \addlyrics {
                %  \set stanza = #"2." Roz -- _ płyń -- cie _ się, me _ źre -- ni -- ce, Tocz -- cie smu -- tnych łez kry -- ni -- ce, Tocz -- _ cie smu -- _ tnych łez _ kry -- ni -- ce.
              %}
              
rightOne =  {
                e'8 dis' fis'4 e'8 dis' cis'4  |
                gis'8 fis' e'4 dis' cis'  |
                gis'4 gis' fis' fis'  |
                gis'4 fis' e' dis'  |
                e'8 dis' fis'4 e'8 dis' cis'4  |
                gis'8 fis' e'4 dis' cis'  |
                \bar "|."
  
}

rightTwo =  {
               cis'2(cis'8) bis cis'4  |
                e'8 dis' cis'4(cis'8) bis gis4 |
                e'2(e'8) dis'16\trill cis' dis'4  |
                e'4 dis' cis'(cis'8) bis  |
                cis'2(cis'8) bis cis'4  |
                e'8 dis' cis'4(cis'8) bis gis4 |
                \bar "|."
  
}

leftOne =  {
                a2 gis2  |
                r1  |
                gis4 b(b)(b8) a  |
                r1  |
                gis4 a gis2  |
                r1  |
                \bar "|."
  
}

leftTwo =  {
                a,2 cis8 dis16 fis16 e8 dis  |
                \ottava #-1
                \set Staff.ottavation = #"8 (oktawę niżej)"
                cis,8 dis, e, fis, gis,4 e, |
                \ottava #0
                cis8 dis e4 b,2 |
                \ottava #-1
                \set Staff.ottavation = #"8"
                cis,8 gis, dis, fis, gis, a, gis,4 |
                \ottava #0
                cis4 fis8 e cis fis16 dis e8 dis |
                \ottava #-1
                \set Staff.ottavation = #"8"
                cis,8 dis, e, fis, gis,4 cis, |
                \bar "|."
}

\score {
  \new PianoStaff <<
    
    \new Staff = "right" \with {
      midiInstrument = "church organ"
    } << 
      \new Voice = "One" { \voiceOne << \globalTempo \global \rightOne >> }
      \new Voice = "Two" { \voiceTwo << \global \rightTwo >> }
    >>
    \new Lyrics \with { alignAboveContext = right } \lyricsto One \verse
    \new Staff = "left" \with {
      midiInstrument = "church organ"
    } { \clef bass << \global \leftOne \\ \global \leftTwo >> }
  >>
  \layout {indent = 0\cm
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
              \context {
                        \Staff
                        \remove "Time_signature_engraver"
                        %\remove "Bar_engraver"
                        %\override Stem #'transparent = ##t
                }
  }
  \midi {
    %\context {
    %  \Score
    %  tempoWholesPerMinute = #(ly:make-moment 60 4)
  %}
  }
}
