\version "2.12.3"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)

\header {
  %composer = "melodia tradycyjna"
    subtitle = \markup{ \medium \fontsize #1.5 "Lament duszy nad cierpiącym Jezusem" }
    tagline = "https://drStypula.pl/gz"
    %title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"
global = {
  \key e \major \autoBeamOff
  %\time 4/4
}
globalTempo = {
    \tempo 4 = 60  \skip \breve.*2 \skip 1*2 \skip 2 
}

verse =  \lyricmode {
               \set stanza = #"1." Je -- zu, na za -- bi -- cie o -- krut -- ne, Ci -- chy Ba -- ran -- ku od wro -- gów szu -- ka -- ny, Je -- zu mój ko -- cha -- ny!
                 Bądź po -- zdro -- wio -- ny, bądź po -- chwa -- lo -- ny, dla nas zel -- żo -- ny i po -- hań -- bio -- ny!
Bądź u -- wiel -- bio -- ny, bądź wy -- sła -- wio -- ny, Bo -- że nie -- skoń -- czo -- ny!  }
              
rightOne =  {
               \cadenzaOn
                e' 8 [fis'] gis' 4 \bar "|"
                gis' 8 gis' a' 4 gis' 8 gis' fis' gis' 4 \bar "|"
                fis' fis' 8 fis' gis' gis' gis' gis'4 fis'8 e' fis' [a'] gis' 4 \bar "|"
                b' 4 b' a' gis' 8 [fis'] b' [a'] gis'4.
                \cadenzaOff
                \bar "||"
                
                \cadenzaOn
                b' 4 a' gis' 8 [fis'] b' [a'] gis' 4 \bar "|"
                b' 4 a' gis' 8 [fis'] b' [a'] gis' 4 \bar "|"
                e' 4 e' fis' fis' 8 [a'] gis' 4 \bar "|"
                e' 4 e' fis' fis' 8 [a'] gis' 4 \bar "|"
                b' 4 a' gis' 8 [fis'] b' [a'] gis' 4 \bar "|"
                b' 4 a' gis' 8 [fis'] b' [a'] gis' 4 \bar "|"
                gis'8^\markup{ \italic rall. } gis' fis' e' fis'[a'] gis' 4 \bar "|."
                \cadenzaOff
}

rightTwo =  {
                e'4(e')
                (e') fis' e' dis'8 e'4
                (e') dis' e'4.(e'4) cis' dis'8 [fis'] e'4
                gis'(gis') fis' dis' fis' e'4.
 
                gis'4 fis' dis' fis' e'
                gis'4 fis' dis' fis' e'
                cis'2 dis'4(dis'8)[fis'] e'4
                cis'2 dis'4(dis'8)[fis'] e'4
                gis'4 fis' dis' fis' e'
                gis'4 fis' dis' fis' e'
                e' cis'8(cis') dis'[fis'] e'4
}


leftOne =  {
                b2
                (b)(b4.)(b4)
                (b2)(b4.) gis2 b4(b)
                e'4(e') cis' b dis' b4.
                
                e'4 cis' b dis' b
                e'4 cis' b dis' b
                gis2 b4(b8)[dis'] b4
                gis2 b4(b8)[dis'] b4
                e'4 cis' b dis' b
                e'4 cis' b dis' b
                b2(b8)[<dis' b>] b4
}

leftTwo =  {
                gis8 [fis] e [dis]
                cis2(cis8) dis4 e
                fis2 e8 [dis] cis(cis2) dis4 e,8 \parenthesize dis,
                cis, [dis,] e,4 fis, gis, b, e,4.

                e,4 fis, gis, fis, e,8 [dis,]
                cis,4 fis, gis, fis, e,8 [dis,]
                cis, [dis,] e,4 fis, b, e,8 [dis,]
                cis, [dis,] e,4 fis, b, e,
                e,4 fis, gis, fis, e,8 [dis,]
                cis,4 fis, gis, fis, e,8 [dis,]
                %\tempo 4 = 55
                cis,4 %\tempo 4 = 50
                b, %\tempo 4 = 30 
                fis, <e, e>
}

\score {
  \new PianoStaff <<
    

    
    \new Staff = "right" \with {
      midiInstrument = "church organ"
    } << 
      \new Voice = "One" { \voiceOne <<     \globalTempo \global \rightOne >> }
      \new Voice = "Two" { \voiceTwo << \global \rightTwo >> }
    >>
    \new Lyrics \with { alignAboveContext = right } \lyricsto One \verse
    
    \new Staff = "left" \with {
      midiInstrument = "church organ"
    } { \clef bass << \global \leftOne \\ \global \leftTwo >> }
  >>
  \layout {indent = 0\cm
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
              \context {
                        \Staff
                        \remove "Time_signature_engraver"
                        %\remove "Bar_engraver"
                        %\override Stem #'transparent = ##t
                }
  }
  \midi {
    %\context {
    %  \Score
    %  tempoWholesPerMinute = #(ly:make-moment 60 4)
  %}
  }
}
