\version "2.12.3"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)

\header {
  %composer = "melodia tradycyjna"
    %subtitle = \markup{ \medium \fontsize #1.5 "Któryś za nas..." }
    tagline = "kawomu.pl | katolicki wortal muzyczny"
    %title = \markup{ \medium \fontsize #3.5 "Gorzkie Żale" }
}

\include "cantate_style.ly"
global = {
  \key e \minor \autoBeamOff
  %\time 4/4
}
globalTempo = {
    \tempo 4 = 45  %\skip \breve.*2 \skip 1*2 \skip 2 
}

verse =  \lyricmode {
               Któ -- ryś za nas cier -- piał ra -- ny, Je -- zu Chry -- ste zmi -- łuj się nad na -- mi.
             }
              
rightOne =  {
                \cadenzaOn
                e'8 fis' e' dis' e' fis' g'4 fis'4 \breathe
                b'4 a' g'8 [fis'] e'4 \breathe
                b'8 b' a' g' fis'4^\markup{ \italic (rit.) } e'4 \bar "|."
                \cadenzaOff
}

rightTwo =  {
                b4 (b) (b) e' dis'4
                g'4 (g'8) [fis'] e' [dis'] g4
                g'4 (g'16) [fis'] e'8 (e'8) [dis'] g4

}


leftOne =  {
                g8 fis g fis g fis b4 (b16) [a16 g \parenthesize fis16]
                d'2 b8. [a16] b,4
                d'4. b8 (b8.) [a16] b,4
                

}

leftTwo =  {
                e8 dis e dis e dis r16 b,32 [a, g, fis,16] r32 b,4
                g,4 d, e,8 [b,] e,4
                e,16 [fis,] g,8 d, e, b,4 e,4 
\bar "|."
}

\score {
  \new PianoStaff <<
    
    
    
    \new Staff = "right" \with {
      midiInstrument = "church organ"
    } << 
      \new Voice = "One" { \voiceOne << \globalTempo \global \rightOne >> }
      \new Voice = "Two" { \voiceTwo << \global \rightTwo >> }
    >>
    \new Lyrics \with { alignAboveContext = right } \lyricsto One \verse
    
    \new Staff = "left" \with {
      midiInstrument = "church organ"
    } { \clef bass << \global \leftOne \\ \global \leftTwo >> }
  >>
  \layout {indent = 0\cm
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
              \context {
                        \Staff
                        \remove "Time_signature_engraver"
                        %\remove "Bar_engraver"
                        %\override Stem #'transparent = ##t
                }
  }
  \midi {
    %\context {
    %  \Score
    %  tempoWholesPerMinute = #(ly:make-moment 60 4)
  %}
  }
}
