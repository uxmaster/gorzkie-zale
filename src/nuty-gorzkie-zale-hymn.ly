% This LilyPond file was downladed from https://uxmaster.gitlab.io/gorzkie-zale/
% generated in Rosegarden and corrected in Frescobaldi
\version "2.12.2"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)


\include "cantate_style.ly"
global = { 
    %\time 2/4
    %\skip \breve.*3  %% 1-1
}
globalTempo = {
    \tempo 4 = 80  \skip \breve.*2 \skip 1*2 \skip 2 
}
\score {
    \new StaffGroup <<
        % force offset of colliding notes in chords:
        \override Score.NoteColumn #'force-hshift = #1.0

        \context Staff = "track 1" << 
           % \set Staff.instrumentName = \markup { \column { "Głos " } }
            \set Score.skipBars = ##t
            \set Staff.printKeyCancellation = ##f
            \new Voice \global
            %\new Voice \globalTempo

            \context Voice = "voice 1" {
                \override Voice.TextScript #'padding = #2.0
                \override MultiMeasureRest #'expand-limit = 1

                \time 1/4
                \clef "treble"
                \key e \major
  \cadenzaOn
  gis' 4 gis' g' gis' 8 _( dis' ) fis' 2 \breathe e' 4 dis' cis' fis' 8 _( e' ) dis' _( cis' ) b 2 \bar ":|" gis' 8 _( b' ) b' 4 a' fis' 8 _( a' ) gis' 2 \breathe gis' 4 fis' e' 4 a' fis' 8 _( a' ) gis' 2 \breathe gis' 4 fis' fis' 8 _( e' ) dis' _( cis' ) b 2  %\bar "|."
                \cadenzaOff
             } % Voice
%              \addlyrics {
%                  Przy -- patrz się, du -- szo, jak cię Bóg mi -- łu -- je, Prze -- cież Go bar -- dziej, niż ka -- tow -- ska drę -- czy, złość two -- ja mę -- czy.
%             }
%             \addlyrics {
%                  Ja -- ko dla cie -- bie so -- bie nie fol -- gu -- je.
%             }
            \addlyrics {
                 \set stanza = #"1." Żal du -- szę ści -- ska, ser -- ce bo -- leść -- czu -- je, Klę -- czy "w O" -- grój -- cu, gdy krwa -- wy pot le -- je, me ser -- ce mdle -- je.
            }
            \addlyrics {
                 Gdy sło -- dki Je -- zus na śmierć się go -- tu -- je;
            }
        >> % Staff (final)
    >> % notes

\layout {indent = 0\cm
\context {
           \Staff
           \remove "Time_signature_engraver"
%           \remove "Bar_engraver"
%           \override Stem #'transparent = ##t
         }
 }
 \midi { }

} % score
